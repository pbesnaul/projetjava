package paul;

public class Vect0<T extends Number> {
	private T[] array;
	
	public Vect0(T[] a) {
		this.array = a;
	}
	
	public String toString() {
		String result = "[";
		for(int i= 0; i<array.length; i++) {
			if(i>0) {
				result += ",";
			}
			
			result += this.array[i];
		}
		result += "]";
		return result;
	}
	
	public T[] getArray() {
		return this.array;
	}
	
	//a.vectSum a+= b
	public void vectSum(Vect0<T> other) {
		for(int i = 0; i<array.length; i++) {
			this.array[i] = (T) ( this.array[i].doubleValue() + other.getArray()[i] ); 
		}
	}
	
	public T vectScalProduct(Vect0<T> other) {
		T t;
		Double total = 0.0d;
		for(int i = 0; i< array.length; i++) {
			total += (this.array()[i].doubleValue() + other.getArray()[i].doubleValue());
		}
		t = (T) new Double(total);
		return t;
	}
	
}

